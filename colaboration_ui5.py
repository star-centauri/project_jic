# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'colaboration.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(424, 341)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("logo.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Form.setWindowIcon(icon)
        Form.setStyleSheet("background-color: rgb(38, 38, 38);\n"
"color: rgb(242, 242, 242);")
        self.horizontalLayoutWidget = QtWidgets.QWidget(Form)
        self.horizontalLayoutWidget.setGeometry(QtCore.QRect(10, 10, 401, 281))
        self.horizontalLayoutWidget.setObjectName("horizontalLayoutWidget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.tabWidget = QtWidgets.QTabWidget(self.horizontalLayoutWidget)
        self.tabWidget.setStyleSheet("QTabBar::tab  {\n"
"    background: rgb(38, 38, 38);\n"
"    border: 1px solid #C4C4C3;\n"
"    border-bottom-color: #C2C7CB; /* same as the pane color */\n"
"    border-top-left-radius: 4px;\n"
"    border-top-right-radius: 4px;\n"
"    min-width: 8ex;\n"
"    padding: 2px;\n"
"}")
        self.tabWidget.setObjectName("tabWidget")
        self.tab_1 = QtWidgets.QWidget()
        self.tab_1.setObjectName("tab_1")
        self.label = QtWidgets.QLabel(self.tab_1)
        self.label.setGeometry(QtCore.QRect(20, 10, 141, 16))
        self.label.setObjectName("label")
        self.label_url = QtWidgets.QLabel(self.tab_1)
        self.label_url.setGeometry(QtCore.QRect(20, 30, 301, 16))
        self.label_url.setText("")
        self.label_url.setObjectName("label_url")
        self.label_timeout = QtWidgets.QLabel(self.tab_1)
        self.label_timeout.setGeometry(QtCore.QRect(20, 70, 221, 16))
        self.label_timeout.setObjectName("label_timeout")
        self.text_information = QtWidgets.QTextBrowser(self.tab_1)
        self.text_information.setGeometry(QtCore.QRect(15, 90, 361, 151))
        self.text_information.setObjectName("text_information")
        self.tabWidget.addTab(self.tab_1, "")
        self.tab_2 = QtWidgets.QWidget()
        self.tab_2.setObjectName("tab_2")
        self.label_3 = QtWidgets.QLabel(self.tab_2)
        self.label_3.setGeometry(QtCore.QRect(10, 10, 71, 16))
        self.label_3.setObjectName("label_3")
        self.edit_add_url = QtWidgets.QLineEdit(self.tab_2)
        self.edit_add_url.setGeometry(QtCore.QRect(10, 30, 371, 20))
        self.edit_add_url.setObjectName("edit_add_url")
        self.list_url = QtWidgets.QListWidget(self.tab_2)
        self.list_url.setGeometry(QtCore.QRect(10, 90, 301, 151))
        self.list_url.setObjectName("list_url")
        self.label_4 = QtWidgets.QLabel(self.tab_2)
        self.label_4.setGeometry(QtCore.QRect(10, 70, 141, 16))
        self.label_4.setObjectName("label_4")
        self.btn_add = QtWidgets.QPushButton(self.tab_2)
        self.btn_add.setGeometry(QtCore.QRect(320, 90, 61, 23))
        self.btn_add.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.btn_add.setStyleSheet("background-color: rgb(13, 13, 13);")
        self.btn_add.setObjectName("btn_add")
        self.btn_remove = QtWidgets.QPushButton(self.tab_2)
        self.btn_remove.setGeometry(QtCore.QRect(320, 120, 61, 23))
        self.btn_remove.setStyleSheet("background-color: rgb(13, 13, 13);")
        self.btn_remove.setObjectName("btn_remove")
        self.tabWidget.addTab(self.tab_2, "")
        self.horizontalLayout.addWidget(self.tabWidget)
        self.horizontalLayoutWidget_2 = QtWidgets.QWidget(Form)
        self.horizontalLayoutWidget_2.setGeometry(QtCore.QRect(10, 300, 401, 31))
        self.horizontalLayoutWidget_2.setObjectName("horizontalLayoutWidget_2")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.horizontalLayoutWidget_2)
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.btn_cancel = QtWidgets.QPushButton(self.horizontalLayoutWidget_2)
        self.btn_cancel.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.btn_cancel.setStyleSheet("background-color: rgb(13, 13, 13);")
        self.btn_cancel.setObjectName("btn_cancel")
        self.horizontalLayout_2.addWidget(self.btn_cancel)
        self.btn_start = QtWidgets.QPushButton(self.horizontalLayoutWidget_2)
        self.btn_start.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.btn_start.setStyleSheet("background-color: rgb(13, 13, 13);")
        self.btn_start.setObjectName("btn_start")
        self.horizontalLayout_2.addWidget(self.btn_start)

        self.retranslateUi(Form)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Observação colaborativa"))
        self.label.setText(_translate("Form", "Servidor do projeto URL:"))
        self.label_timeout.setText(_translate("Form", "Timeout:"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_1), _translate("Form", "Endereço URL"))
        self.label_3.setText(_translate("Form", "Adicionar URL:"))
        self.label_4.setText(_translate("Form", "Grupo observação:"))
        self.btn_add.setText(_translate("Form", "Adicionar"))
        self.btn_remove.setText(_translate("Form", "Remover"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_2), _translate("Form", "Criar grupo "))
        self.btn_cancel.setText(_translate("Form", "Cancelar"))
        self.btn_start.setText(_translate("Form", "Começar"))

